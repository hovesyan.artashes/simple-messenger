export const state = () => {
  return {
    items: state
  };
};

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items;
  },
};

export const actions = {
  async getItems ({ commit }) {
    const { data } = await this.$axios.get('/messeges');
    commit('SET_ITEMS', data);
  },
  async addItem ({ commit }, messege) {
    await this.$axios.post('/messeges',messege)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }
}

export const getters = {
  items: (state) => state.items,
};
