export const state = () => {
  return {
    user: {
      name: ''
    }
  };
};

export const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },
};

export const actions = {
  async setUser ({ commit,state }, name) {
    const user = {name: name};
    commit('SET_USER', user);
  }
}

export const getters = {
  user: (state) => state.user,
};
