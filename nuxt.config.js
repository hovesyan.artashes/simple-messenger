module.exports = {

  head: {
    title: 'simple-messenger',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: false,

  css: ['@/assets/scss/app.scss'],

  plugins: ['~plugins/bootstrap-vue'],

  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    baseURL: 'https://task-manager.hovessian.com',
  },


  build: {
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
